declare module 'ganalytics' {
    declare class GAnalytics {
        constructor(accountID: string): this;
        send(type: string, parameters: mixed): void;
    }

    declare export default typeof GAnalytics;
};
