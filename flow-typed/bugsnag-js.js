declare module 'bugsnag-js' {
    declare function bugsnag (key: string): Function;

    declare export default typeof bugsnag;
};
