declare module 'stickybits' {
    declare function stickybits (selector: string, options: Object): void;

    declare export default typeof stickybits;
};
