declare module 'quicklink' {
    declare type Options = {
        timeout?: number,
        priority?: boolean,
        timeoutFn?: Function,
        el?: HTMLElement
    };

    declare function quickLink (options?: Options): void;

    declare export default typeof quickLink;
};
