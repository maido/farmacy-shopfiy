declare module 'image-promise' {
    declare export default function Promise(image: string | HTMLImageElement): Promise<HTMLImageElement>
};
