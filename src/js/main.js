/**
 * @prettier
 * @flow
 */
// import quickLink from 'quicklink';
import backgroundVideo from './components/background-video';
import hasJS from './components/has-js';
import bookATable from './components/book-a-table';
import cartPreview from './components/cart-preview';
import {init as modalInit} from './components/modal';
import overlayNavigation from './components/overlay-navigation';
// import restaurantsNavigation from './components/restaurants-navigation';
// import pageTransitions from './components/page-transitions';
import scrollMonitor from './components/scroll-monitor';
import sticky from './components/sticky';
import tracking from './components/tracking';
import visibilityToggle from './components/visibility-toggle';
import '../sass/main.scss';

const init = () => {
    hasJS.init();
    tracking.init();
    overlayNavigation.init();
    // restaurantsNavigation.init();
    bookATable.init();
    scrollMonitor.init();
    visibilityToggle.init();
    cartPreview.init();
    sticky.init();
    backgroundVideo.init();
    modalInit();
    // quickLink();
    // pageTransitions.init();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
