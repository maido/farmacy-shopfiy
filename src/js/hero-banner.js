/**
 * @prettier
 * @flow
 */
import clamp from 'lodash/clamp';

const MAX_ROTATION = 3;
const MAX_SCROLL = 500;

let $heroBannerEdge;
let currentRotation = MAX_ROTATION;

const handleWindowScroll = () => {
    const isWithinMaxScroll = window.scrollY < MAX_SCROLL;
    const isWithinMaxRotation = currentRotation >= 0 && currentRotation <= MAX_ROTATION;

    if ($heroBannerEdge && isWithinMaxScroll && isWithinMaxRotation) {
        currentRotation = clamp(window.scrollY / 185, 0, MAX_ROTATION);

        $heroBannerEdge.style.transform = `rotate(${MAX_ROTATION - currentRotation}deg)`;
    }
};

const cacheElements = () => {
    $heroBannerEdge = document.querySelector('.js-hero-banner-edge');
};

const addEvents = () => {
    if ($heroBannerEdge) {
        window.addEventListener('scroll', handleWindowScroll, false);
    }
};

const init = () => {
    cacheElements();
    addEvents();
    handleWindowScroll();

    if ($heroBannerEdge) {
        $heroBannerEdge.classList.add('is-active');
    }
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
