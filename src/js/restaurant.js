/**
 * @prettier
 * @flow
 */
import USContent from './components/us-content';

const init = () => {
    // USContent.init();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
