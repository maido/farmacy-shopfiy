/**
 * @prettier
 * @flow
 */
export const SPACING = 24;
export const SPACING_SMALL = 12;
export const SPACING_TINY = 8;

export const ANIMATION_DURATION = 600;
export const ANIMATION_EASING = 'easeOutExpo';
export const ANIMATION_ELASTICITY = ANIMATION_DURATION / 3;
