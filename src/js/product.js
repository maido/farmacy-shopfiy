/**
 * @prettier
 * @/flow
 */
import Sticky from 'sticky-js';
import product from './components/product';
import tabList from './components/tab-list';
import {isMobile} from './services/helpers';
import '../sass/product.scss';

const stickySetup = () => {
    if (typeof window !== 'undefined' && document.body) {
        const meetsThreshold =
            Math.round((document.body.scrollHeight / window.innerHeight) * 100) >= 150;

        if (!isMobile() && meetsThreshold) {
            new Sticky('.js-sticky');
        }
    }
};

const init = () => {
    product.init();
    tabList.init();

    stickySetup();
};

if (window.isModernBrowser) {
    window.addEventListener('load', init);
}
