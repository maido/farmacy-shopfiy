/**
 * @prettier
 * @flow
 */
import anime from 'animejs';
import throttle from 'lodash/throttle';

let $logo;
let $nav;
let $stickyItem;
let $stickyThreshold;

const stickyClass: string = 'u-is-stuck';
const stickyScrollOffset: number = 50;
let stickyThresholdValue: number = 35;
let isStuck: boolean = false;

const addSticky = () => {
    if ($stickyItem) {
        $stickyItem.classList.add(stickyClass);
        isStuck = true;

        if ($stickyItem.classList.contains('c-site-header--hide-logo')) {
            if ($logo) {
                anime({
                    easing: 'spring(2, 200, 25, 0)',
                    opacity: [0, 1],
                    targets: $logo,
                    translateX: ['-100%', 0]
                });
            }

            if ($nav) {
                anime({
                    easing: 'spring(2, 200, 25, 0)',
                    targets: $nav,
                    translateX: [(190 + 34) * -1, 0]
                });
            }
        }
    }
};

const removeSticky = () => {
    if ($stickyItem) {
        $stickyItem.classList.remove(stickyClass);
        isStuck = false;
    }

    if ($stickyItem.classList.contains('c-site-header--hide-logo')) {
        if ($logo) {
            anime({
                easing: 'spring(1, 100, 25, 0)',
                opacity: [1, 0],
                targets: $logo,
                translateX: [0, '-100%']
            });
        }

        if ($nav) {
            anime({
                easing: 'spring(1, 100, 25, 0)',
                targets: $nav,
                translateX: [0, (190 + 34) * -1]
            });
        }
    }
};

const handleStickyChange = () => {
    const offset: number = window.pageYOffset;

    if (offset > stickyThresholdValue && !isStuck) {
        addSticky();
    } else if (isStuck && offset <= stickyThresholdValue + stickyScrollOffset) {
        removeSticky();
    }
};
const handleWindowScroll = throttle(handleStickyChange, 250);

export const destroySticky = () => {
    removeSticky();

    window.removeEventListener('scroll', handleWindowScroll);
    window.removeEventListener('touchmove', handleWindowScroll);
};

const cacheElements = () => {
    $logo = ((document.querySelector('.js-logo'): any): HTMLElement);
    $nav = ((document.querySelector('.js-nav'): any): HTMLElement);
    $stickyItem = ((document.querySelector('.js-sticky'): any): HTMLElement);
    $stickyItem = ((document.querySelector('.js-sticky'): any): HTMLElement);
    $stickyThreshold = ((document.querySelector('.js-sticky-threshold'): any): HTMLElement);
};

const addEvents = () => {
    window.addEventListener('scroll', handleWindowScroll, false);
    window.addEventListener('touchmove', handleWindowScroll, false);
};

const init = () => {
    cacheElements();

    if ($stickyItem) {
        addEvents();
        handleStickyChange();
    }
};

export default {init};
