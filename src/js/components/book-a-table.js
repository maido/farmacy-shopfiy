/**
 * @prettier
 * @flow
 */
import {closeModal, init as modalInit, openModal, setActiveModal} from './modal';

let $ctaButtons;
let $iframe;
let $modalCloseButton;

export const trackOpen = (url: string) => {
    if (window.ga) {
        window.ga('send', 'event', {
            eventCategory: 'Book a table',
            eventAction: 'open',
            eventLabel: url
        });
    }
};

const handleButtonClick = (event: Event) => {
    const $target = ((event.target: any): HTMLLinkElement);

    if ($target) {
        event.preventDefault();

        if ($iframe) {
            $iframe.setAttribute('src', $target.href);
        }

        setActiveModal('book-a-table');
        openModal(true);
        trackOpen($target.href);
    }
};

const handleModalCloseClick = () => {
    setActiveModal('book-a-table');
    closeModal();
    setActiveModal('');

    if ($iframe) {
        $iframe.setAttribute('src', '');
    }
};

const addEvents = () => {
    if ($ctaButtons) {
        [...$ctaButtons].map($button =>
            $button.addEventListener('click', handleButtonClick, false)
        );
    }

    if ($modalCloseButton) {
        $modalCloseButton.addEventListener('click', handleModalCloseClick, false);
    }
};

const cacheElements = () => {
    $ctaButtons = ((document.querySelectorAll('.js-book-a-table'): any): NodeList<HTMLLinkElement>);
    $iframe = ((document.querySelector('.js-book-a-table-iframe'): any): HTMLIFrameElement);
    $modalCloseButton = ((document.querySelector(
        '.js-modal-book-a-table-close'
    ): any): HTMLButtonElement);
};

const init = () => {
    cacheElements();

    if ($iframe && $ctaButtons.length > 0) {
        addEvents();
        modalInit();
    }
};

export default {init};
