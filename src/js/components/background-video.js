/**
 * @prettier
 * @flow
 */
import scrollMonitor from 'scrollmonitor';
import {isMobile} from '../services/helpers';
let $videos;

const handleVideoInViewport = ($video: HTMLVideoElement) => {
    const src = $video.getAttribute('data-src');

    if (src) {
        const $sources = $video.querySelectorAll('source');

        if ($sources.length) {
            [...$sources].map($source => $source.setAttribute('src', src));
        }

        $video.setAttribute('src', src);
        $video.play();

        setTimeout(() => $video.classList.add('is-active'), 250);
    }
};

const setupVideos = () => {
    if ($videos.length) {
        [...$videos].map($video => {
            const monitor = scrollMonitor.create($video, -300);

            monitor.enterViewport(() => handleVideoInViewport($video));
        });
    }
};

const cacheElements = () => {
    $videos = ((document.querySelectorAll('.js-bg-video'): any): NodeList<HTMLVideoElement>);
};

const init = () => {
    if (!isMobile()) {
        cacheElements();
        setupVideos();
    }
};

export default {init};
