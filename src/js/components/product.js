/**
 * @prettier
 * @flow
 */
import find from 'lodash/find';
import get from 'lodash/get';
import {
    addToCart,
    getCart,
    init as cartInit,
    showCartSummaryModal,
    trackAddToCart,
    updateCartSummary
} from './cart';
import {updateHistory} from '../services/helpers';

type AddToCartButtonProperties = {
    label?: string,
    'product-handle'?: string,
    quantity?: string,
    'variant-id'?: string
};

const product = get(window, 'globalData.product');
const relatedProducts = get(window, 'globalData.relatedProducts');
let $addButtons;
let $quantityInputs;
let $variantSelectButtons;
let $variantSelects;

const trackProductView = (id: number, price: number) => {
    if (window.fbq && id && price) {
        window.fbq('track', 'ViewContent', {
            content_ids: [parseInt(id)],
            content_type: 'product',
            currency: 'GBP',
            value: parseInt(price) / 100
        });
    }
};

// const isSoldOut = (product: ShopifyProduct) => {
//     const productAvailable = product.available;
//     const variantAvailable = get(product, 'variants[0].available');

//     return !productAvailable | !variantAvailable;
// };

const updateAddButtons = (properties: AddToCartButtonProperties, productHandle = '') => {
    if ($addButtons) {
        [...$addButtons]
            .filter($button => {
                if (productHandle !== '') {
                    return $button.dataset.productHandle === productHandle;
                } else {
                    return true;
                }
            })
            .map($button => {
                Object.keys(properties).map(property =>
                    $button.setAttribute(`data-${property}`, properties[property])
                );

                const variant = find(product.variants, {id: parseInt(properties['variant-id'])});

                if (variant) {
                    if (variant.available) {
                        $button.removeAttribute('disabled');
                        $button.innerText = 'Add to cart';
                    } else {
                        $button.setAttribute('disabled', 'disabled');
                        $button.innerText = 'Sold out';
                    }
                }
            });
    }
};

const updateVariantButtons = (selectedVariantId: string) => {
    if ($variantSelectButtons) {
        [...$variantSelectButtons].map($button => {
            $button.classList.toggle(
                'c-button--selected',
                $button.dataset.variantId === selectedVariantId
            );
        });
    }
};

const getProductStatus = (productHandle: string, variantId: number) => {
    fetch(`/products/${productHandle}.js`, {
        credentials: 'same-origin',
        headers: {'content-type': 'application/json'}
    })
        .then(response => response.json())
        .then(response => updateAddButtons({'variant-id': `${variantId}`}, productHandle));
};

const addProductToCart = (productHandle: string, variantId: number, quantity: number) => {
    return new Promise((resolve, reject) => {
        addToCart(variantId, quantity)
            .then(getCart)
            .then(cartData => {
                updateCartSummary(cartData);
                showCartSummaryModal(cartData, variantId, relatedProducts);
                // getProductStatus(productHandle, variantId);
                resolve();
            })
            .catch(() => (window.location.href = '/cart'));
    });
};

const handleAddToCartClick = (event: Event) => {
    const $target = ((event.target: any): HTMLElement);

    if ($target) {
        event.preventDefault();
        $target.blur();

        const {price, productHandle, quantity, source, variantId} = get($target, 'dataset');

        if (productHandle && variantId) {
            let previousLabel = $target.innerText;

            $target.innerText = 'Adding...';

            addProductToCart(productHandle, parseInt(variantId), parseInt(quantity))
                .then(() => {
                    trackAddToCart(productHandle, variantId, price, source);
                    $target.innerText = 'Added!';
                    setTimeout(() => ($target.innerText = 'Add another'), 1000);
                })
                .catch(() => ($target.innerText = previousLabel));
        }
    }
};

const updateURLWithNewProduct = (handle: string): void => updateHistory(`/products/${handle}`);

const handleVariantButtonClick = (event: Event) => {
    const $target = ((event.target: any): HTMLButtonElement);

    if ($target) {
        updateVariantButtons($target.dataset.variantId);
        updateAddButtons({'variant-id': $target.dataset.variantId}, product.handle);
        updateURLWithNewProduct(`${product.handle}?variant=${$target.dataset.variantId}`);
    }
};

const handleVariantSelectChange = (event: Event) => {
    const $target = ((event.target: any): HTMLSelectElement);

    if ($target) {
        updateAddButtons({'variant-id': $target.value}, product.handle);
        updateURLWithNewProduct(`${product.handle}?variant=${$target.value}`);
    }
};

const handleQuantityChange = (event: Event) => {
    const $target = ((event.target: any): HTMLInputElement);

    if ($target) {
        updateAddButtons({quantity: $target.value});
    }
};

const addEvents = () => {
    if ($addButtons) {
        [...$addButtons].map($button => $button.addEventListener('click', handleAddToCartClick));
    }

    if ($quantityInputs) {
        [...$quantityInputs].map($input => $input.addEventListener('change', handleQuantityChange));
    }

    if ($variantSelectButtons) {
        [...$variantSelectButtons].map($button =>
            $button.addEventListener('click', handleVariantButtonClick)
        );
    }

    if ($variantSelects) {
        [...$variantSelects].map($select =>
            $select.addEventListener('change', handleVariantSelectChange)
        );
    }
};

const cacheElements = () => {
    $addButtons = ((document.querySelectorAll(
        '.js-add-to-cart'
    ): any): NodeList<HTMLButtonElement>);
    $quantityInputs = ((document.querySelectorAll(
        '.js-variant-quantity'
    ): any): NodeList<HTMLInputElement>);
    $variantSelectButtons = ((document.querySelectorAll(
        '.js-variant-button'
    ): any): NodeList<HTMLButtonElement>);
    $variantSelects = ((document.querySelectorAll(
        '.js-variant-select'
    ): any): NodeList<HTMLSelectElement>);
};

const init = () => {
    cacheElements();
    addEvents();
    cartInit();
};

export default {init};
