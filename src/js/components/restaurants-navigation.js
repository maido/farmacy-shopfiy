/**
 * @prettier
 * @flow
 */
import anime from 'animejs';
import get from 'lodash/get';
import throttle from 'lodash/throttle';
import {isMobile} from '../services/helpers';

const ITEMS = get(window, 'globalData.restaurants', []);
let isActive = false;
let $container;
let $header;
let $subNavLinks;

const buildNav = () => {
    if ($container) {
        const html = ITEMS.map(
            item => `
            <div class="o-layout__item u-6/12@mobile">
                <div class="u-flex">
                    <a href="${item.url}" class="u-pr+ u-image-hover ${
                item.url === '' ? 'u-no-pointer' : ''
            }">
                        <img src="${item.photo}" alt="" srcset="${
                item.photoHiRes
            } 2x" style="max-height: 250px;">
                    </a>
                    <div>
                        <a href="${
                            item.url
                        }" class="u-heading u-heading--caps u-display-block u-h3 u-mb u-pt u-underline-hover ${
                item.url === '' ? 'u-no-pointer' : ''
            }">${item.title.replace(', ', ' <span>')}</span></a>
                        <p class="u-max-w-350px">${item.description}</p>
                    </div>
                </div>
            </div>
        `
        ).join('');

        $container.innerHTML = `<div class="o-wrapper o-wrapper--huge">
            <div class="o-layout o-layout--large">${html}</div>
        </div>`;
    }
};

const handeSubNavLinkEnter = (event: Event) => {
    const $target = ((event.target: any): HTMLElement);

    if ($target && !isActive) {
        console.log('enter', $target);
        const $subNav = (($target.nextElementSibling: any): HTMLElement);

        if ($subNav) {
            isActive = true;

            if ($header) {
                $header.classList.add('u-is-stuck');
            }

            anime({
                begin() {
                    $subNav.style.visibility = 'visible';
                },
                easing: 'spring(1, 100, 25, 0)',
                opacity: [0, 1],
                targets: $subNav,
                translateY: [-48, 0]
            });
        }
    }
};

const handeSubNavLinkLeave = (event: Event) => {
    const $target = ((event.target: any): HTMLElement);

    if ($target && isActive) {
        console.log('leave', $target);
        const $subNav = (($target.nextElementSibling: any): HTMLElement);

        if ($subNav) {
            isActive = false;

            anime({
                complete() {
                    $subNav.style.visibility = 'hidden';
                },
                easing: 'spring(1, 100, 25, 0)',
                opacity: [1, 0],
                targets: $subNav,
                translateY: [0, -48]
            });

            if ($header) {
                $header.classList.remove('u-is-stuck');
            }
        }
    }
};

const addEvents = () => {
    if ($subNavLinks) {
        [...$subNavLinks].map($link => {
            $link.addEventListener('mouseenter', handeSubNavLinkEnter, false);
            $link.addEventListener('mouseleave', (event: Event) => {
                setTimeout(() => handeSubNavLinkLeave(event), 500);
            });
        });
    }
};

const cacheElements = () => {
    $container = ((document.querySelector('.js-restaurants-nav'): any): HTMLElement);
    $header = ((document.querySelector('.js-site-header'): any): HTMLElement);
    $subNavLinks = ((document.querySelectorAll('.js-sub-nav-link'): any): NodeList<HTMLElement>);
};

const init = () => {
    if (!isMobile() && ITEMS.length) {
        cacheElements();
        buildNav();
        // addEvents();
    }
};

export default {init};
