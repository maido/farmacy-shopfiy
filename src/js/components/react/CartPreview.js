/**
 * @prettier
 * @/flow
 */
import {h, Fragment} from 'preact';
import {useEffect, useState} from 'preact/hooks';
import {getPrice} from '../../services/helpers';

type Props = {data: ShopifyCart};

const filteredRecommendedItems = (recommendedItems: Array<Object>, cart: ShopifyCart) => {
    const itemsInCart = cart.items.map(item => item.product_title);

    return recommendedItems.filter((item, index) => index < 3 && !itemsInCart.includes(item.title));
};

const CartPreview = ({initialData}: Props) => {
    const [data, setData] = useState(initialData);
    const [highlightedVariantId, setHighlightedVariantId] = useState(0);
    const [recommendedItems, setRecommendedItems] = useState([]);

    const updateCart = (
        data: ShopifyCart,
        highlightedVariantId: number,
        recommendedItems: Array<Object> = []
    ) => {
        setData(data);
        setHighlightedVariantId(highlightedVariantId);
        setRecommendedItems(filteredRecommendedItems(recommendedItems, data));
    };

    useEffect(() => {
        window.globalComponents.updateCartPreview = updateCart;
    });

    const viewRecommendedProduct = (handle: string) => (window.location = `/products/${handle}`);

    return (
        <div>
            <h2 className="u-heading u-heading--caps u-h3  u-color-primary u-mb">
                Your <span>cart</span>
            </h2>

            {data.items.length > 0 && (
                <Fragment>
                    <div className="c-modal__scroller">
                        {data.items.length > 0 &&
                            data.items.map(item => (
                                <div
                                    className={`u-flex u-align-items-center ${
                                        item.variant_id === highlightedVariantId
                                            ? 'u-highlight-change'
                                            : ''
                                    }`}
                                    key={item.title}
                                >
                                    <a href={item.url}>
                                        <img
                                            src={item.image}
                                            alt={`${item.title} product image`}
                                            className="u-pr"
                                            style={{
                                                objectFit: 'cover',
                                                height: 100,
                                                width: 100
                                            }}
                                        />
                                    </a>
                                    <div className="u-flex-grow-1">
                                        <a
                                            href={item.url}
                                            className="u-h5 u-heading u-display-block"
                                        >
                                            {item.product_title}
                                        </a>
                                        {item.variant_title && (
                                            <span className="u-heading u-heading--caps u-color-primary">
                                                {item.variant_title}&nbsp;&nbsp;
                                            </span>
                                        )}
                                        {item.quantity > 1 && (
                                            <span class="u-color-secondary">
                                                x{item.quantity} at {getPrice(item.price)}
                                            </span>
                                        )}
                                        {item.quantity === 1 && (
                                            <span class="u-color-secondary">
                                                {getPrice(item.price)}
                                            </span>
                                        )}
                                    </div>
                                </div>
                            ))}
                    </div>
                    {recommendedItems.length > 0 && (
                        <div className="u-p u-mt u--mb t-tertiary">
                            <span className="u-label u-color-primary">You might also like</span>

                            <div className="u-flex u-mt-" style={{marginLeft: -12}}>
                                {recommendedItems.map(item => (
                                    <div
                                        className="u-text-align-center u-ml-"
                                        onClick={() => viewRecommendedProduct(item.handle)}
                                    >
                                        <div
                                            style={{
                                                backgroundColor: '#efefef',
                                                backgroundImage: `url(${item.image})`,
                                                backgroundPosition: 'center center',
                                                backgroundSize: 'cover',
                                                cursor: 'pointer',
                                                width: 120,
                                                height: 140
                                            }}
                                            className="u-mb- u-mh-auto"
                                        />
                                        <small>{item.title}</small>
                                    </div>
                                ))}
                            </div>
                        </div>
                    )}

                    <footer className="u-mt u-mt+@mobile u-text-align-right c-animated-item u-in-viewport">
                        <h3 className="u-h4" data-order="2">
                            <span className="u-heading u-heading--caps">Subtotal</span>{' '}
                            {getPrice(data.total_price)}
                        </h3>
                        <small data-order="3">
                            Shipping is <span>calculated</span> during checkout.{' '}
                        </small>

                        <div className="c-button-list c-button-list--small u-mt-">
                            <a
                                href="/cart"
                                className="c-button c-button--secondary u-1/1"
                                data-order="4"
                            >
                                Edit cart
                            </a>
                            <a
                                href="/checkout"
                                className="c-button c-button--primary u-1/1"
                                data-order="5"
                            >
                                Checkout
                            </a>
                        </div>
                    </footer>
                </Fragment>
            )}
            {data.items.length === 0 && (
                <Fragment>
                    <p>You have no items in your cart.</p>
                    <footer className="u-mt">
                        <a href="/products" className="c-button c-button--secondary u-1/1">
                            Continue browsing
                        </a>
                    </footer>
                </Fragment>
            )}
        </div>
    );
};

export default CartPreview;
