/**
 * @prettier
 * @flow
 */
import get from 'lodash/get';
import Cookie from 'js-cookie';
import formats from './currency-formats';

const SHOPIFY_COOKIE_NAME = 'cart_currency';
const USER_COOKIE_NAME = 'user_currency';

const currencies = get(window, 'globalData.currencies');
const shopifyCurrency = Cookie.get(SHOPIFY_COOKIE_NAME);
const userCurrency = Cookie.get(USER_COOKIE_NAME);

let $selects;
let $prices;

const setCurrency = currency => Cookie.set(USER_COOKIE_NAME, currency, {expires: 365});

/**
 * Copied from Shopify's Slate tools.
 */
const formatWithDelimiters = (number = 0, precision = 2, thousands = ',', decimal = '.') => {
    if (isNaN(number) || number == null) {
        return 0;
    }

    number = (parseInt(number) / 100.0).toFixed(precision);

    const parts = number.split('.');
    const dollarsAmount = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
    const centsAmount = parts[1] && parts[1] !== '00' ? decimal + parts[1] : '';

    return dollarsAmount + centsAmount;
};

/**
 * Copied from Shopify's Slate tools.
 */
const formatMoney = (cents, format) => {
    if (typeof cents === 'string') {
        cents = cents.replace('.', '');
    }

    const moneyFormat = '£{{amount}} GBP';
    const placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
    const formatString = format || moneyFormat;
    const match = formatString.match(placeholderRegex);
    let value = '';

    if (match && match.length >= 2) {
        switch (match[1]) {
            case 'amount':
                value = formatWithDelimiters(cents, 2);
                break;
            case 'amount_no_decimals':
                value = formatWithDelimiters(cents, 0);
                break;
            case 'amount_with_comma_separator':
                value = formatWithDelimiters(cents, 2, '.', ',');
                break;
            case 'amount_no_decimals_with_comma_separator':
                value = formatWithDelimiters(cents, 0, '.', ',');
                break;
        }

        if (currencies.roundPrice) {
            value = Math.round(parseFloat(value));
        }

        return formatString.replace(placeholderRegex, value.toString());
    }
};

const convertAllPrices = newCurrency => {
    if ($prices) {
        const defaultCurrency = shopifyCurrency || 'GBP';

        [...$prices].map($price => {
            if (newCurrency !== defaultCurrency) {
                const newPrice = window.Currency.convert(
                    parseFloat($price.dataset.defaultPrice),
                    defaultCurrency,
                    newCurrency
                );
                const formattedPrice = formatMoney(newPrice, formats[newCurrency].money_format);

                if (formattedPrice) {
                    $price.innerHTML = formattedPrice;
                    $price.setAttribute('data-currency', newCurrency);
                    $price.setAttribute('data-price', `${newPrice}`);
                }
            } else {
                $price.innerHTML = $price.dataset.defaultPriceWithCurrency;
                $price.setAttribute('data-currency', $price.dataset.defaultCurrency);
                $price.setAttribute('data-price', $price.dataset.price);
            }
        });
    }
};

const setSelectedCurrency = (currency = '') => {
    if ($selects) {
        [...$selects].map($select => ($select.value = currency));
    }
};

const handleCurrencyChange = (event: Event) => {
    const $target = ((event.target: any): HTMLSelectElement);

    if ($target) {
        const newCurrency = $target.value;

        convertAllPrices(newCurrency);
        setCurrency(newCurrency);
    }
};

const addEvents = () => {
    if ($selects) {
        [...$selects].map($select => $select.addEventListener('change', handleCurrencyChange));
    }
};

const cacheElements = () => {
    $prices = ((document.querySelectorAll('.js-price'): any): NodeList<HTMLElement>);
    $selects = ((document.querySelectorAll(
        '.js-currency-select'
    ): any): NodeList<HTMLSelectElement>);
};

const updateWithUserCurrency = () => {
    if (userCurrency !== shopifyCurrency) {
        convertAllPrices(userCurrency);
        setSelectedCurrency(userCurrency);
    }
};

export const init = () => {
    if (window.hasOwnProperty('Currency')) {
        cacheElements();
        addEvents();
        updateWithUserCurrency();
    }
};

export default {init};
