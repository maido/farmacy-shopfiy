/**
 * @prettier
 * @flow
 */
import anime from 'animejs';
import tracking, {track} from './tracking';
import {intendedTargetElement} from '../services/helpers';
import {ANIMATION_DURATION, ANIMATION_EASING, SPACING_TINY} from '../config';

let $visibilityToggleItems;

const trackToggleDisplay = (toggleLabel: string, toggleId: string, isVisible: boolean) => {
    const label = `${toggleLabel} | ${toggleId}`;

    track('event', {
        ec: 'Element display toggle',
        ea: isVisible ? 'Visible' : 'Hidden',
        el: label
    });
};

export const setItemInvisible = ($toggle: Object, targetSelector: string) => {
    const $target = document.querySelector(targetSelector);

    if ($target && $toggle) {
        anime({
            complete: () => {
                $toggle.setAttribute('aria-expanded', 'false');
                $target.setAttribute('aria-hidden', 'true');
                $target.setAttribute('style', '');
            },
            duration: ANIMATION_DURATION / 3,
            easing: ANIMATION_EASING,
            height: 0,
            opacity: 0,
            targets: $target
        });
    }
};

export const setItemVisible = ($toggle: Object, targetSelector: string) => {
    const $target = document.querySelector(targetSelector);

    if ($target && $toggle) {
        $target.style.visibility = 'hidden';
        $target.style.display = 'block';

        const elementHeight = $target.getBoundingClientRect().height;

        $target.style.visibility = 'visible';
        $target.style.height = '0px';

        $toggle.setAttribute('aria-expanded', 'true');
        $target.setAttribute('aria-hidden', 'false');

        anime({
            duration: ANIMATION_DURATION / 3,
            easing: 'easeOutExpo',
            height: elementHeight,
            opacity: 1,
            round: true,
            targets: $target,
            translateY: [SPACING_TINY, 0]
        });

        trackToggleDisplay($toggle.innerText, $toggle.id, true);
    }
};

const hideItem = (selector: string) => {
    if (selector) {
        const $targets = document.querySelectorAll(selector);

        if ($targets) {
            anime({
                duration: ANIMATION_DURATION / 3,
                easing: 'easeOutExpo',
                height: 0,
                opacity: 0,
                round: true,
                targets: $targets
            });
        }
    }
};

const handleVisibilityToggleClick = (event: UIEvent) => {
    if (event.target) {
        event.preventDefault();

        const $target: HTMLSelectElement = (event.target: any);
        const $toggle = ((intendedTargetElement(
            'js-visibility-toggle',
            $target
        ): any): HTMLElement);

        if ($toggle) {
            const toggleIsExpanded = $toggle.getAttribute('aria-expanded');
            const targetSelector = $toggle.getAttribute('aria-controls');
            const targetHides = $toggle.dataset.hides;

            if (targetSelector) {
                if (toggleIsExpanded === 'false') {
                    setItemVisible($toggle, `#${targetSelector ? targetSelector : ''}`);

                    if (targetHides) {
                        hideItem(targetHides);
                    }
                } else {
                    setItemInvisible($toggle, `#${targetSelector ? targetSelector : ''}`);
                }
            }
        }
    }
};

const cacheElements = () => {
    $visibilityToggleItems = document.querySelectorAll('.js-visibility-toggle');
};

const addEvents = () => {
    if ($visibilityToggleItems) {
        [...$visibilityToggleItems].map($toggle => {
            $toggle.addEventListener('click', handleVisibilityToggleClick, false);
        });
    }
};

const init = () => {
    tracking.init(false);
    cacheElements();
    addEvents();
};

export default {init};
