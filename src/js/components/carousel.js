/**
 * @prettier
 * @/flow
 */
import Flickity from 'flickity';
import {isMobile} from '../services/helpers';

let $carousels;

const setFullHeightItems = ($items: Array<HTMLElement>) => {
    if ($items && $items.length) {
        [...$items].map($item => ($item.style.height = '100%'));
    }
};

const carouselOptions = {
    gallery: {
        autoPlay: 3000,
        friction: 0.3,
        pageDots: true,
        selectedAttraction: 0.02,
        wrapAround: true
    },
    menus: {
        cellAlign: 'left',
        friction: 0.3,
        on: {
            ready: () => {
                const $items = document.querySelectorAll('.c-menu');

                if ($items.length) {
                    setFullHeightItems($items);
                }
            }
        },
        pageDots: true,
        prevNextButtons: false,
        selectedAttraction: 0.02
    },
    products: {
        cellAlign: 'left',
        friction: 0.3,
        on: {
            ready: function ready() {
                if (this.cells.length) {
                    const $items = this.cells.map(c => c.element);

                    [...$items].map($item => $item.classList.remove('u-show@mobile'));
                    setFullHeightItems($items);
                    this.reloadCells();
                }
            }
        },
        pageDots: true,
        prevNextButtons: false,
        selectedAttraction: 0.02,
        wrapAround: true
    },
    layoutItems: {
        adaptiveHeight: false,
        cellAlign: 'left',
        friction: 0.3,
        pageDots: true,
        prevNextButtons: true,
        selectedAttraction: 0.02
    },
    quotes: {
        autoPlay: 3000,
        cellAlign: 'left',
        friction: 0.3,
        on: {
            ready: function ready() {
                if (this.cells.length) {
                    const $items = this.cells.map(c => c.element);

                    [...$items].map($item => $item.classList.remove('u-hide'));
                    setFullHeightItems($items);
                    this.reloadCells();
                }
            }
        },
        pageDots: true,
        prevNextButtons: false,
        selectedAttraction: 0.02,
        wrapAround: true
    }
};

const cacheElements = () => {
    $carousels = ((document.querySelectorAll('.js-carousel'): any): NodeList<HTMLVideoElement>);
};

const setup = () => {
    if ($carousels.length) {
        [...$carousels].map($carousel => {
            if ($carousel.children.length > 1) {
                const option = $carousel.dataset.carouselOptions || 'gallery';
                let carouselEnabled = true;

                if ($carousel.dataset.carouselMobile && !isMobile()) {
                    carouselEnabled = false;
                }

                if (carouselEnabled) {
                    const carousel = new Flickity($carousel, carouselOptions[option]);
                }
            }
        });
    }
};

const init = () => {
    cacheElements();
    setup();
};

export default {init};
