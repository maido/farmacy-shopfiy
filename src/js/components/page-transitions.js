/**
 * @prettier
 * @/flow
 */
import barba from '@barba/core';
import anime from 'animejs';

const init = () => {
    barba.init({
        transitions: [
            {
                leave(data) {
                    const done = this.async();
                    const $container = data.current.container;

                    if ($container) {
                        $container.style.backgroundColor = 'red';
                        setTimeout(done, 1500);
                    } else {
                        done();
                    }
                },
                enter(data) {
                    const done = this.async();
                    const $container = data.current.container;

                    if ($container) {
                        $container.style.backgroundColor = '';
                        setTimeout(done, 1500);
                    } else {
                        done();
                    }
                }
            }
        ]
    });
};

export default {init};
