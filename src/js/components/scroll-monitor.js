/**
 * @prettier
 * @flow
 */
import scrollMonitor from 'scrollmonitor';

let $items;

export const SCROLL_MONITOR_OFFSET = -150;

const handleItemEnterViewport = (event, item) => {
    const element = item.watchItem;

    element.classList.add('u-in-viewport');

    if (element.tagName === 'IMG' && element.dataset.src) {
        element.src = element.dataset.src;
    }
};

const createMonitors = () => {
    if ($items) {
        [...$items].map($item => {
            const monitor = scrollMonitor.create($item, SCROLL_MONITOR_OFFSET);

            monitor.enterViewport(handleItemEnterViewport);
        });
    }
};

const cacheElements = () => {
    $items = ((document.querySelectorAll('.js-monitor'): any): NodeList<HTMLElement>);
};

const init = () => {
    cacheElements();
    createMonitors();
};

export default {init};
