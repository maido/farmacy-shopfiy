/**
 * @prettier
 * @flow
 */
import React from 'react';
import {render} from 'react-dom';
import anime from 'animejs';
import eq from 'lodash/eq';
import get from 'lodash/get';
import fromPairs from 'lodash/fromPairs';
import isEmpty from 'lodash/isEmpty';
import map from 'lodash/map';
import replace from 'lodash/replace';
import {closeModal, init as modalInit, openModal, setActiveModal} from './modal';
import {
    getFetchParams,
    getPrice,
    handleFetchErrors,
    intendedTargetElement
} from '../services/helpers';
import {ANIMATION_DURATION} from '../config';

let $cartCount;

let cart = get(window, 'globalData.cart');
let cartLocked = false;
let cartSummaryModalItemTemplate;

const lockCart = () => (cartLocked = true);
const unlockCart = () => (cartLocked = false);

const shopifyRequest = (url: string, data?: Object): Promise<*> => {
    return new Promise((resolve, reject) => {
        if (!cartLocked) {
            lockCart();

            fetch(url, getFetchParams(data))
                .then(response => response.json())
                .then(response => {
                    unlockCart();
                    resolve(response);
                })
                .catch(error => {
                    unlockCart();
                    reject(error);
                });
        }
    });
};

const handleCartRefresh = () => window.location.reload();

export const trackAddToCart = (handle: string, id: number, value: string, source: string) => {
    if (window.ga) {
        window.ga('send', 'event', {
            eventCategory: 'Add to cart',
            eventAction: source,
            eventLabel: handle
        });
    }

    if (window.fbq) {
        window.fbq('track', 'AddToCart', {
            content_ids: [parseInt(id)],
            content_type: 'product',
            currency: 'GBP',
            value: parseInt(value) / 100
        });
    }
};

export const getCart = () => shopifyRequest('/cart.js');

export const updateCartSummary = (data: ShopifyCart) => {
    if ($cartCount) {
        [...$cartCount].map($count => ($count.innerText = `${data.item_count}`));
    }
};

export const showCartSummaryModal = (
    data: ShopifyCart,
    highlightedVariantId: number,
    relatedProducts: Array<Object> = []
) => {
    const updateCartPreview = get(window, 'globalComponents.updateCartPreview');

    if (updateCartPreview) {
        updateCartPreview(data, highlightedVariantId, relatedProducts);
        setActiveModal('cart-preview');
        openModal(true);
    }
};

export const addToCart = (id: number, quantity: number = 1) =>
    shopifyRequest('/cart/add.js', {id, quantity});

const cacheElements = () => {
    $cartCount = ((document.querySelectorAll('.js-cart-count'): any): NodeList<HTMLElement>);
};

export const init = () => {
    cacheElements();
    modalInit();
};

export default {init};
