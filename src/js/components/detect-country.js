/**
 * @prettier
 * @flow
 */
const IPINFODB_API_KEY = process.env.IPINFODB_API_KEY ? process.env.IPINFODB_API_KEY : '';

const getCountryCode = (): Promise<string> => {
    return new Promise((resolve, reject) => {
        fetch(`https://api.ipinfodb.com/v3/ip-country/?key=${IPINFODB_API_KEY}&format=json`)
            .then(response => response.json())
            .then(({countryCode}) => {
                if (countryCode) {
                    resolve(countryCode);
                } else {
                    reject('Country unknown');
                }
            });
    });
};

export default getCountryCode;
