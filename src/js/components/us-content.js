/**
 * @prettier
 * @flow
 */
import getCountryCode from './detect-country';

const US_COUNTRY_CODE = 'US';

const init = () => {
    getCountryCode().then(countryCode => {
        if (countryCode === US_COUNTRY_CODE) {
            alert(`country is '${countryCode}' and should show US priority content`);
        } else {
            alert(`country is '${countryCode}' and should show UK priority content`);
        }
    });
};

export default {init};
