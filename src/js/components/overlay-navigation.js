/**
 * @prettier
 * @flow
 */
import anime from 'animejs';

let $body;
let $labels;
let $links;
let $overlay;
let $overlayInner;
let $siteHeader;
let $toggleButton;
let isVisible = false;

const hideOverlay = () => {
    if ($body && $labels && $links && $overlay && $overlayInner && $siteHeader && $toggleButton) {
        anime({
            begin() {
                $body.classList.remove('u-hide-body', 's-overlay-visible');
                $toggleButton.classList.remove('c-hamburger--active');
            },
            complete() {
                isVisible = false;
                $overlay.style.display = 'none';
            },
            delay: 300,
            duration: 200,
            easing: 'linear',
            opacity: [1, 0],
            targets: $overlay
        });

        anime.timeline().add({
            easing: 'spring(2, 200, 25, 0)',
            delay: (target, index) => index * 25,
            opacity: [1, 0],
            targets: $links,
            translateY: [0, '100%']
        });
    }
};

const showOverlay = () => {
    if ($body && $labels && $links && $overlay && $overlayInner && $siteHeader && $toggleButton) {
        anime({
            begin() {
                isVisible = true;
                $overlay.style.display = 'flex';
                $body.classList.add('u-hide-body', 's-overlay-visible');
                $toggleButton.classList.add('c-hamburger--active');

                anime({
                    easing: 'spring(2, 200, 25, 0)',
                    delay: (target, index) => index * 50,
                    opacity: [0, 1],
                    targets: $links,
                    translateY: ['100%', 0]
                });
            },
            duration: 200,
            easing: 'linear',
            opacity: [0, 1],
            targets: $overlay
        });
    }
};

const handleHamburgerClick = (event: Event) => {
    if ($siteHeader && $toggleButton) {
        if (isVisible === false) {
            showOverlay();
        } else {
            hideOverlay();
        }
    }

    event.preventDefault();
};

const addEvents = () => {
    if ($toggleButton) {
        $toggleButton.addEventListener('click', handleHamburgerClick, false);
    }
};

const cacheElements = () => {
    $body = ((document.querySelector('body'): any): HTMLElement);
    $labels = ((document.querySelectorAll('.js-overlay-nav-label'): any): NodeList<HTMLElement>);
    $links = ((document.querySelectorAll(
        '.js-overlay-nav-link span'
    ): any): NodeList<HTMLLinkElement>);
    $siteHeader = ((document.querySelector('.js-site-header'): any): HTMLElement);
    $overlay = ((document.querySelector('.js-overlay-nav'): any): HTMLElement);
    $overlayInner = ((document.querySelector('.js-overlay-nav-inner'): any): HTMLElement);
    $toggleButton = ((document.querySelector('.js-overlay-nav-toggle'): any): HTMLElement);
};

const init = () => {
    cacheElements();
    addEvents();
};

export default {init};
